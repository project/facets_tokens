<?php

/**
 * @file
 * Builds placeholder replacement tokens for facet-related data.
 */

/**
 * Implements hook_token_info().
 *
 * @see facetapi_token_info()
 */
function facets_tokens_token_info() {
  $types = $tokens = $facets = array();

  foreach (facetapi_get_searcher_info() as $searcher) {
    $facets += facetapi_get_facet_info($searcher['name']);
  }

  if ($facets) {
    $types['facet'] = array(
      'name' => t('Facets'),
      'description' => t('Tokens related to facets.'),
    );

    $types['first-facet'] = array(
      'name' => t('First active facet'),
      'description' => t('Tokens related to the first active facet.'),
      'type' => 'active-facet',
    );

    $types['last-facet'] = array(
      'name' => t('Last active facet'),
      'description' => t('Tokens related to the last active facet.'),
      'type' => 'active-facet',
    );

    $types['active-facet'] = array(
      'name' => t('Active facet'),
      'description' => t('Tokens related to the active facet.'),
      'needs-data' => 'facet_items',
    );

    // Add tokens for each facet.
    foreach ($facets as $facet_name => $facet) {
      $facet_name = str_replace(':', '-', $facet_name);

      $tokens['facet'][$facet_name] = array(
        'name' => $facet['label'],
        'description' => $facet['description'],
        'type' => 'active-facet',
      );
    }

    $tokens['active-facet']['value'] = array(
      'name' => t('Mapped value'),
      'description' => t('The mapped value of the active facet.'),
      'dynamic' => TRUE,
    );

    $tokens['active-facet']['raw'] = array(
      'name' => t('Raw value'),
      'description' => t('The raw value of the active facet as stored in the index.'),
      'dynamic' => TRUE,
    );

    $tokens['active-facet']['label'] = array(
      'name' => t('Facet label'),
      'description' => t('The human readable label of the active facet.'),
    );

    $tokens['active-facet']['name'] = array(
      'name' => t('Facet name'),
      'description' => t('The machine readable name of the active facet.'),
    );
  }

  return array(
    'types' => $types,
    'tokens' => $tokens,
  );
}

/**
 * Implements hook_tokens().
 *
 * @see facetapi_tokens()
 */
function facets_tokens_tokens($type, $tokens, array $data = array(), array $options = array()) {
  $replacements = array();

  if ($type == 'facet' && $items = facets_tokens_get_active_items()) {
    foreach ($items as $facet_name => $facet_items) {
      $facet_name = str_replace(':', '-', $facet_name);

      if ($facet_tokens = token_find_with_prefix($tokens, $facet_name)) {
        $replacements += token_generate('active-facet', $facet_tokens, array('facet_items' => $facet_items), $options);
      }
    }
  }

  elseif ($type == 'first-facet' && $items = facets_tokens_get_active_items()) {
    $replacements += token_generate('active-facet', $tokens, array('facet_items' => reset($items)), $options);
  }

  elseif ($type == 'last-facet' && $items = facets_tokens_get_active_items()) {
    $replacements += token_generate('active-facet', $tokens, array('facet_items' => end($items)), $options);
  }

  elseif ($type == 'active-facet' && !empty($data['facet_items'])) {
    $sanitize = !empty($options['sanitize']);

    foreach ($tokens as $name => $original) {
      $parts = explode(':', $name, 2);
      $items = $data['facet_items'];

      // Check the item index.
      if (isset($parts[1])) {
        list($name, $index) = $parts;

        // Only integer index is allowed.
        if (!is_numeric($index) || intval($index) != $index) {
          continue;
        }

        // A negative index count back from the end of the array,
        // and an index of -1 refers to the last item.
        if ($index < 0) {
          $index = count($items) + $index;
        }

        // Reduce the array to a single item.
        if (isset($items[$index])) {
          $items = array($items[$index]);
        }
        else {
          continue;
        }
      }

      $adapter = $items[0]['adapter'];
      $facet_name = $items[0]['facets'][0];

      // Map values if necessary.
      if ($name == 'value') {
        foreach ($items as &$item) {
          $item['value'] = $adapter->getMappedValue($facet_name, $item['value'])['#markup'];
        }
      }

      switch ($name) {
        case 'value':
        case 'raw':
          $delimiter = variable_get('facets_tokens_value_delimiter', ', ');
          $values = implode($delimiter, array_column($items, 'value'));
          $replacements[$original] = $sanitize ? check_plain($values) : $values;
          break;

        case 'label':
          if ($facet = facetapi_facet_load($facet_name, $adapter->getSearcher())) {
            $replacements[$original] = $sanitize ? check_plain($facet['label']) : $facet['label'];
          }
          break;

        case 'name':
          $replacements[$original] = $sanitize ? check_plain($facet_name) : $facet_name;
          break;
      }
    }
  }

  return $replacements;
}
