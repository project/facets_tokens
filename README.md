# Facets Tokens

Provides tokens for facet-related data.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/facets_tokens).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/facets_tokens).


## Requirements

This module requires the following modules:

- [Facet API](https://www.drupal.org/project/facetapi)
- [Token](https://www.drupal.org/project/token)


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see [Installing Drupal Modules](https://www.drupal.org/docs/7/extend/installing-modules).


## Configuration

When enabled, the module adds new global token types:

- `facet`: Tokens related to facets.
- `first-facet`: Tokens related to the first active facet.
- `last-facet`: Tokens related to the last active facet.

The module has only one setting, which can be set via [Drush](https://www.drush.org):

```bash
drush vset facets_tokens_value_delimiter " "
```

This allows you to change the default delimiter for facet values. The default
delimiter is `, `.


## Maintainers

- Andrey Tymchuk ([WalkingDexter](https://www.drupal.org/u/walkingdexter))
- Gevorg Mkrtchyan ([armrus](https://www.drupal.org/u/armrus))
- Vadim Bardachev ([vbard](https://www.drupal.org/u/vbard))

Ongoing development is sponsored by [Initlab](https://www.drupal.org/initlab).
